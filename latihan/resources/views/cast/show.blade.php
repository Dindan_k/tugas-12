@extends('layout.master')
@section('judul')
    Table
@endsection
@push('style')
    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/v/bs4/dt-1.11.5/datatables.min.css"/>
@endpush
@push('script')
    <script src="{{asset('layout/plugins/datatables/jquery.dataTables.js')}}"></script>
    <script src="{{asset('layout/plugins/datatables-bs4/js/dataTables.bootstrap4.js')}}"></script>
    <script>
      $(function () {
        $("#example1").DataTable();
  });
    </script>
@endpush
@section('content')
    <h1>{{$cast->nama}}</h1>
    <p>{{$cast->umur}}</p>
    <p>{{$cast->bio}}</p>
@endsection
