@extends('layout.master');
@section('judul')
    Halaman Tambah Data
@endsection

@section('content')
<form action="/cast" method="POST">
    @csrf
    <div class="form-group">
      <label for="nama">Nama</label>
      <input type="text" class="form-control" id="nama" name="nama">
    </div>
    @error('nama')
      <div class="alert alert-danger">{{ $message }}</div>
    @enderror
    <div class="form-group">
      <label for="umur">Umur</label>
      <input type="text" class="form-control" id="umur" name="umur">
    </div>
    @error('umur')
      <div class="alert alert-danger">{{ $message }}</div>
    @enderror
    <div class="form-group">
      <label for="bio">Bio</label>
      <textarea class="form-control" id="bio" rows="3" name="bio"></textarea>
    </div>
    @error('bio')
      <div class="alert alert-danger">{{ $message }}</div>
    @enderror
    <button type="submit" class="btn btn-primary">Simpan</button>
</form>
@endsection
