@extends('layout.master')
@section('judul')
    Table
@endsection
@push('style')
    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/v/bs4/dt-1.11.5/datatables.min.css"/>
@endpush
@push('script')
    <script src="{{asset('layout/plugins/datatables/jquery.dataTables.js')}}"></script>
    <script src="{{asset('layout/plugins/datatables-bs4/js/dataTables.bootstrap4.js')}}"></script>
    <script>
      $(function () {
        $("#example1").DataTable();
  });
    </script>
@endpush
@section('content')
<a href="/cast/create" type="submit" class="btn btn-primary">+ Tambah Data</a>
<table class="table">
    <thead class="thead-dark">
    <tr>
      <th>No</th>
      <th>Nama</th>
      <th>Umur</th>
      <th>Biodata</th>
      <th>action</th>
    </tr>
    </thead>
    <tbody>
        @forelse ($cast as $key => $item)
        <tr>
            <td>{{ $loop->iteration }}</td>
            <td>{{ $item->nama }}</td>
            <td>{{ $item->umur }}</td>
            <td>{{ $item->bio }}</td>
            <td>

                <form action="/cast/{{$item->id}}" method="POST">
                <a href="/cast/{{$item->id}}" class="btn btn-info btn-sm">Details</a>
                <a href="/cast/{{$item->id}}/edit" class="btn btn-success btn-sm">Edit</a>
                    @csrf
                    @method('delete')
                    <input type="submit" class="btn btn-danger btn-sm" value="Delete">
                </form>
            </td>
        </tr>
        @empty
        <H1>Data Kosong</H1>
        @endforelse
    </tbody>
</table>
@endsection
