@extends('layout.master')
@section('judul')
    Halaman Utama
@endsection
@section('content')
    <h1>Sanberbook</h1>
    <h3>Sosial Media Developer Santai Berkualitas</h3>
    <p>Belajar dan berbagi agar hidup menjadi lebih baik</p>
    <h4>Benefit Join di Saberbook</h4>
    <ul>
        <li>Mendapatkan motivasi dari sesama para Developer</li>
        <li>Sharing Knowlenge</li>
        <li>Dibuat oleh calon web Developer terbaik</li>
    </ul>
    <h4>Cara Bergabung ke Sanberbook</h4>
    <ol>
        <li>Mengunjungi Website ini</li>
        <li>Mendaftarkan di <a href="/register">Form Sign Up</a></li>
        <li>Selesai</li>
    </ol>
@endsection
