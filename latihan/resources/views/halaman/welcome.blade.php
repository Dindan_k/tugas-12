@extends('layout.master')
@section('judul')
    Halaman Selamat Datang
@endsection
@section('content')
    <h1>SELAMAT DATANG {{ $first_name }} {{ $last_name }}</h1>
    <h3>Terima kasih telah bergabung di Sanberbook. Media Belajar kita bersama!</h3>
@endsection
