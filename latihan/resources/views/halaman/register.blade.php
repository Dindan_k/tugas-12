@extends('layout.master')
@section('judul')
    Halaman Register
@endsection
@section('content')
    <h3>Buat Account Baru</h3>
    <h4>Sign Up Form</h4>
    <form action="/welcome" method="POST">
        @csrf
        <label for="first_name">First Name :</label><br>
        <input type="text" id="first_name" name="first_name" required><br><br>
        <label for="last_name">Last Name :</label><br>
        <input type="text" id="last_name" name="last_name" required><br><br>
        <label>Gender </label><br><br>
        <input type="radio" name="gender" value="1">Male <br>
        <input type="radio" name="gender"  value="1">Female <br>
        <input type="radio" name="gender"  value="3">Other <br><br>
        <label for="">Nationality</label><br><br>
        <select name="nationality">
            <option value="indonesia">Indoensia</option>
            <option value="malaysia">Amerika</option>
            <option value="vietnam">Inggris</option>
        </select><br><br>
        <label for="">Language Spoken</label><br><br>
        <input type="checkbox" name="language">Bahasa Indonesia <br>
        <input type="checkbox" name="language">English <br>
        <input type="checkbox" name="language">Other <br><br>
        <label for="bio">Bio</label><br><br>
        <textarea name="bio" id="bio" cols="30" rows="10" required></textarea><br>
        <input type="submit" value="Sign Up">
    </form>
@endsection
